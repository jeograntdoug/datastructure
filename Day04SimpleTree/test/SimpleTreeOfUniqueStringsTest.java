/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import day04simpletree.SimpleTreeOfUniqueStrings;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author heokc
 */
public class SimpleTreeOfUniqueStringsTest {
    
    public SimpleTreeOfUniqueStringsTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void addNodeIntoTree(){
        SimpleTreeOfUniqueStrings tree = new SimpleTreeOfUniqueStrings();
        tree.add("g");
        tree.add("f");
        tree.add("h");
        tree.add("d");
        tree.add("j");
        tree.add("s");
        tree.add("a");
        assertEquals(7,tree.size());
        
        assertTrue(tree.has("g"));
        assertTrue(tree.has("f"));
        assertTrue(tree.has("h"));
        assertTrue(tree.has("d"));
        assertTrue(tree.has("j"));
        assertTrue(tree.has("s"));
        assertTrue(tree.has("a"));
        
        assertFalse(tree.has("z"));
    }
    
    @Test
    public void removeNodeFromTree(){
        SimpleTreeOfUniqueStrings tree = new SimpleTreeOfUniqueStrings();
        tree.add("g");
        tree.add("f");
        tree.add("h");
        tree.add("d");
        tree.add("j");
        tree.add("s");
        tree.add("a");
        assertEquals(7,tree.size());
        
        //Delete root node, end node, middle node
        tree.remove("g");
        assertEquals(6,tree.size());
        assertFalse(tree.has("g"));
        tree.remove("d");
        assertEquals(5,tree.size());
        assertFalse(tree.has("d"));
        tree.remove("f");
        assertEquals(4,tree.size());
        assertFalse(tree.has("f"));
        tree.remove("h");
        assertEquals(3,tree.size());        
        assertFalse(tree.has("h"));
        
        // Add node again
        tree.add("g");
        tree.add("f");
        assertEquals(5,tree.size());
        
        // Exception test
        try{
            tree.remove("1");
            assertFalse("remove method doesn't throw IllegalArgumentException",true);
        } catch (IllegalArgumentException ex){
            assertTrue("remove method throws IllegalArgumentException",true);
        }
    }
}
