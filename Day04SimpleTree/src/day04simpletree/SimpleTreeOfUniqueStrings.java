package day04simpletree;

public class SimpleTreeOfUniqueStrings {

    private class Node {

        public Node(String value) {
            this.value = value;
        }
        String value;
        Node left, right, parent;

        @Override
        public String toString() {
            return String.format("[%s]", value);
        }
    }

    private Node root;
    private int nodesTotal;

    public void add(String value) {
        if (nodesTotal == 0) {
            root = new Node(value);
            nodesTotal++;
            return;
        }

        Node node = findNodeByValue(root, value);

        Node newNode = new Node(value);

        if (node.value.equals(value)) {
            throw new IllegalArgumentException();
        } else if (value.compareTo(node.value) < 0) {
            node.left = newNode;
        } else {
            node.right = newNode;
        }

        newNode.parent = node;
        nodesTotal++;

    } // throws IllegalArgumentException if value already present

    /**
     * looking for node that has value. if there is node with value, returns the
     * node if there is no node in tree, returns null else return parent node
     *
     *
     * @param node
     * @param value value that we are looking for
     * @return
     */
    private Node findNodeByValue(Node node, String value) {
        if (node == null) {   
            return null;
        }

        Node childNode;

        if (value.compareTo(node.value) == 0) {
            return node;
        } else if (value.compareTo(node.value) < 0) {
            childNode = findNodeByValue(node.left, value);
        } else {
            childNode = findNodeByValue(node.right, value);
        }

        if (childNode == null) {
            return node;
        } else {
            return childNode;
        }
    }

    public boolean has(String value) {
        Node node = findNodeByValue(root, value);
        if (node == null) {
            return false;
        }

        if (!value.equals(node.value)) {
            return false;
        }

        return true;
    }

    public void remove(String value) {
        if (nodesTotal == 0) {
            throw new IllegalArgumentException();
        }

        Node node = findNodeByValue(root, value);

        if (node == null) {
            System.out.println("Internal Error");
            return;
        }

        if (!value.equals(node.value)) {
            throw new IllegalArgumentException();
        }

        Node parent = node.parent;
        Node lNode = node.left;
        Node rNode = node.right;

        Node idxNode = node;
        Node newChildNode = null;

        if (lNode != null) {
            newChildNode = lNode;

            idxNode = lNode;
            while (idxNode.right != null) {
                idxNode = idxNode.right;
            }

            idxNode.right = rNode;
            if (rNode != null) {
                rNode.parent = newChildNode;
            }
        } else if (rNode != null) {
            newChildNode = rNode;
        }

        if (newChildNode != null) {
            newChildNode.parent = parent;
        }

        //find node direction from parent node
        if (parent == null) {
            root = newChildNode;
        } else if (node.value.compareTo(parent.value) < 0) {
            parent.left = newChildNode;
        } else {
            parent.right = newChildNode;
        }
        nodesTotal--;
    } // HARD! throws IllegalArgumentException if value does not exist

    public int size() {
        return nodesTotal;
    }

    private int recursiveToArray(Node node, int count, String[] strArray) {
        if (node == null) {
            return count;
        }
        
        if (node.left != null) {
            count = recursiveToArray(node.left, count, strArray);
        }

        System.out.printf("value: %s , count: %d\n",node.value,count);
        strArray[count++] = node.value;;

        if (node.right != null) {
            count = recursiveToArray(node.right, count, strArray);
        }
        return count;
    }

    public String[] toArray() {
        String[] dataArray = new String[nodesTotal];
        Integer initialCount = 0;
        recursiveToArray(root, initialCount, dataArray);
        return dataArray;
    } // allocate array of nodesTotal, recursively visit all nodes of the tree

    public String[] toArraySorted() {
        return toArray();
    } // HARD! Traverse from left-most low to right-most low

    public void printDebug() {
    } // any output that helps you with debugging, recursively visit all nodes of the trees

    @Override
    public String toString() {
        return String.format("[%s]",String.join(",",toArray()));
    } // result like for ArrayList: "[Value1,Value2,Value3]"
    
    
    public void printAll(){
        printNodes(root);
    }
    private void printNodes(Node node){
        
        
        //check left node
        if(node.left != null){
            
            printNodes(node.left);
            //print left
//            System.out.printf(" %s ",node.left.value);
        }
        
        //print middle
        System.out.printf(" %s ",node.value);
        
        
        //check right node
        if(node.right != null){
            printNodes(node.right);
            //print right
//            System.out.printf(" %s ",node.right.value);
        }
        
    }
}
