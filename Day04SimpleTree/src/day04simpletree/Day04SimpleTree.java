
package day04simpletree;

public class Day04SimpleTree {

    public static void main(String[] args) {
        SimpleTreeOfUniqueStrings tree = new SimpleTreeOfUniqueStrings();
        tree.add("g");
        tree.add("f");
        tree.add("h");
        tree.add("d");
        tree.add("j");
        tree.add("s");
        tree.add("a");
        System.out.println(String.join(", ",tree.toArray()));
        System.out.println("");
        tree.printAll();
    }
}
