/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz1rollingfifo;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1898918
 */
public class RollingPriorityFIFOTest {
    
    RollingPriorityFIFO fifo;
    int size = 6;
    
    public RollingPriorityFIFOTest() {
    }
       
    @Before
    public void setUp() {
        
        fifo = new RollingPriorityFIFO(size);
        assertEquals(size,fifo.sizeMax());
        assertEquals(0,fifo.size());
    }
    
    @After
    public void tearDown() {
     
    }

    
    /**
     * Only for connection test
     * DELETE getStart(), getEnd() methods,
     * Modify public to private in Item inner class
     * after Test is done
     */

    @Test
    public void isInputSizeGreaterThanMinimumSize(){
        try{
            RollingPriorityFIFO test = new RollingPriorityFIFO(4);
            
            assertTrue("This message shouldn't show",false);
        } catch (IllegalArgumentException ex){
            assertTrue("IllegalArgumentException is thrown",true);
        }
    }
    
    @Test
    public void enqueueValueInTheQueue(){
        
        try{
            fifo.enqueue("a", true);
            assertEquals(1,fifo.size());
            fifo.enqueue("b", false);
            assertEquals(2,fifo.size());
            fifo.enqueue("c", true);
            assertEquals(3,fifo.size());
            fifo.enqueue("d", false);
            assertEquals(4,fifo.size());
            fifo.enqueue("e", true);
            assertEquals(5,fifo.size());
            fifo.enqueue("f", false);
            assertEquals(6,fifo.size());
            
            fifo.enqueue("z", false);
            assertTrue("This message shouldn't show",false);
        } catch (FIFOFullException ex){
            assertTrue("FIFOFullException is thrown",true);
            assertEquals(6,fifo.size());
        }
    }
    
    @Test(timeout = 1000)
    public void dequeueFromQueue(){
        try{
            // case 1 : 1 total & dequeue
            fifo.enqueue("a", false);
            assertEquals("a",fifo.dequeue());
            assertEquals(0,fifo.size());
            
            
            // case 2 : more than 4 total dequeue;
            fifo.enqueue("a", true);
            fifo.enqueue("b", false);
            fifo.enqueue("c", false);
            fifo.enqueue("d", true);
            fifo.enqueue("e", true);
            assertEquals(5,fifo.size());
            
            //// 2-1 dequeue start of queue with priority
            assertEquals("a",fifo.dequeue());
            assertEquals(4,fifo.size());
            
            //// 2-2 dequeue middle of queue with priority
            assertEquals("d",fifo.dequeue());
            assertEquals(3,fifo.size());
            //// 2-3 dequeue end of queue with priority
            assertEquals("e",fifo.dequeue());
            assertEquals(2,fifo.size());
            
            //// 2-4 dequeue item without priority
            assertEquals("b",fifo.dequeue());
            assertEquals(1,fifo.size());
            assertEquals("c",fifo.dequeue());
            assertEquals(0,fifo.size());
            
        
        
        } catch (FIFOFullException ex){
            assertFalse("This must not showing",true);
        }
    }
    
    @Test(timeout = 1000)
    public void enqueueAndDequeueMultipleTimes(){
        
        try {
            assertNull(fifo.dequeue());
            
            //=========No priority===========
            // enqueue 5 then dequeue 5
            fifo.enqueue("a", false);
            fifo.enqueue("b", false);
            fifo.enqueue("c", false);
            fifo.enqueue("d", false);
            fifo.enqueue("e", false);
            fifo.enqueue("f", false);
            assertEquals(6,fifo.size());
            
            fifo.dequeue();
            fifo.dequeue();
            fifo.dequeue();
            fifo.dequeue();
            fifo.dequeue();
            fifo.dequeue();
            assertEquals(0,fifo.size());
            
            assertNull(fifo.dequeue());
            
            // enqueue 3 then dequeue 3
            fifo.enqueue("a", false);
            fifo.enqueue("b", false);
            fifo.enqueue("c", false);
            assertEquals(3,fifo.size());
            
            fifo.dequeue();
            fifo.dequeue();
            fifo.dequeue();
            assertEquals(0,fifo.size());
            
            //=========with all priority===========
            // enqueue 5 then dequeue 5
            fifo.enqueue("a", true);
            fifo.enqueue("b", true);
            fifo.enqueue("c", true);
            fifo.enqueue("d", true);
            fifo.enqueue("e", true);
            fifo.enqueue("f", true);
            assertEquals(6,fifo.size());
            
            fifo.dequeue();
            fifo.dequeue();
            fifo.dequeue();
            fifo.dequeue();
            fifo.dequeue();
            fifo.dequeue();
            assertEquals(0,fifo.size());
            
            assertNull(fifo.dequeue());
            
            // enqueue 3 then dequeue 3
            fifo.enqueue("a", true);
            fifo.enqueue("b", true);
            fifo.enqueue("c", true);
            assertEquals(3,fifo.size());
            
            fifo.dequeue();
            fifo.dequeue();
            fifo.dequeue();
            assertEquals(0,fifo.size());
            
            
            //=========with some priority===========
            // enqueue 5 then dequeue 5
            fifo.enqueue("a", true);
            fifo.enqueue("b", false);
            fifo.enqueue("c", true);
            fifo.enqueue("d", false);
            fifo.enqueue("e", false);
            fifo.enqueue("f", true);
            assertEquals(6,fifo.size());
            
            fifo.dequeue();
            fifo.dequeue();
            fifo.dequeue();
            fifo.dequeue();
            fifo.dequeue();
            fifo.dequeue();
            assertEquals(0,fifo.size());
            
            assertNull(fifo.dequeue());
            
            // enqueue 3 then dequeue 3
            fifo.enqueue("a", true);
            fifo.enqueue("b", false);
            fifo.enqueue("c", false);
            assertEquals(3,fifo.size());
            
            fifo.dequeue();
            fifo.dequeue();
            fifo.dequeue();
            assertEquals(0,fifo.size());
            
        } catch (FIFOFullException ex) {
            assertFalse("This must not showing",true);
        }
    }
    
    @Test(timeout = 1000)
    public void getValuesAsArray(){
        try {
            fifo.enqueue("a", false);
            fifo.enqueue("b", false);
            fifo.enqueue("c", false);
            fifo.enqueue("d", false);
            fifo.enqueue("e", false);
            assertEquals(5,fifo.size());
            
            assertArrayEquals(new String[]{"e","d","c","b","a"}, fifo.toArray());
            
        } catch (FIFOFullException ex) {
            assertFalse("This must not showing",true);
        }
    }
    
    @Test
    public void getOnlyPriorityValuesAsArray(){
        try {
            fifo.enqueue("a", false);
            fifo.enqueue("b", true);
            fifo.enqueue("c", true);
            fifo.enqueue("d", false);
            fifo.enqueue("e", true);
            assertEquals(5,fifo.size());
            assertArrayEquals(new String[]{"e","c","b"}, fifo.toArrayOnlyPriority());
            
        } catch (FIFOFullException ex) {
            assertFalse("This must not showing",true);
        }
    }
    
    @Test
    public void getValuesString(){
        try {
            fifo.enqueue("a", false);
            fifo.enqueue("b", true);
            fifo.enqueue("c", true);
            fifo.enqueue("d", false);
            fifo.enqueue("e", true);
            assertEquals(5,fifo.size());
            assertEquals("[e*, d, c*, b*, a]", fifo.toString());
            
        } catch (FIFOFullException ex) {
            assertFalse("This must not showing",true);
        }
    }
}
