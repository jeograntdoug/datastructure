package quiz1rollingfifo;

class FIFOFullException extends Exception {
    public FIFOFullException(){
        super();
    }
}

public class RollingPriorityFIFO {

    private class Item {
        Item next, prev;
        boolean priority;
        String value;
    }

    private Item start, end;
    int itemsTotal, itemsCurrUsed;

    private final int MIN_QUEUE_SIZE = 5;
    
    /* Parameter itemsTotal must be 5 or more, otherwise IllegalArgumentException
    * is thrown. Items are allocated and connected via next/prev pointer only
    * once - here, in the constructor. After that they are re-used.
     */
    public RollingPriorityFIFO(int itemsTotal) {
        if(itemsTotal < MIN_QUEUE_SIZE){
            throw new IllegalArgumentException();
        }
        createQueue(itemsTotal);
        this.itemsTotal = itemsTotal;
    }
    
    private void createQueue(int size){
        start = new Item();
        end = start;
        Item idxItem = start;
        
        for(int i=0 ; i < size - 1; i++){
            Item newItem = new Item();
            
            idxItem.next = newItem;
            newItem.prev = idxItem;
            
            idxItem = idxItem.next;
        }
        idxItem.next = start;
        start.prev = idxItem;
    }
    
   
    // Places value in the next available Item. If FIFO is full throws exception.
    public void enqueue(String value, boolean priority) throws FIFOFullException {
        if(itemsCurrUsed == itemsTotal){
            throw new FIFOFullException();
        }
        
        //Special case : empty queue
        if(itemsCurrUsed == 0){
            start.value = value;
            start.priority = priority;
            itemsCurrUsed++;
            return;
        }
        
        end.prev.value = value;
        end.prev.priority = priority;
        end = end.prev;
        itemsCurrUsed++;
    }

    /* returns null if fifo is empty, if it is not emtpy then
    * priority=true items are sarched first
    * if none is found then non-priority item is returned
     */
    public String dequeue() {
        if(itemsCurrUsed == 0){return null;}
        if(itemsCurrUsed == 1){
            String value = start.value;
            start.value = null;
            itemsCurrUsed = 0;
            return value;
        }
        
        Item idxItem = start;
        boolean isPriority = false;
        for(int i = 0; i < itemsCurrUsed ; i++){
            if(idxItem.priority){
                isPriority = true;
                break;
            }
            idxItem = idxItem.prev;
        }
        
        //dequeue start item
        if(!isPriority || idxItem == start){
            idxItem = start;
            start = idxItem.prev;
        
        }else{
            if(idxItem == end){// dequeue end item
                end = idxItem.next;
            } else {// dequeue middle item
                relocateItem(idxItem);
            }
        }
        
        itemsCurrUsed--;
        String value = idxItem.value;
        idxItem.value = null;
        return value;
    }
    
    // take off item from queue
    private void relocateItem(Item item){
        item.prev.next = item.next;
        item.next.prev = item.prev;
        
        //insert item outside of queue
        Item endPrev = end.prev;
        end.prev = item;
        item.next = end;
        
        item.prev = endPrev;
        endPrev.next = item;
    }

    public int size() {
        return itemsCurrUsed;
    } // current FIFO size

    public int sizeMax() {
        return itemsTotal;
    } // maximum FIFO size

    // Returns array of Strings of all items in FIFO.
    public String[] toArray() {
        
        String[] itemArray = new String[itemsCurrUsed];
        Item idxItem = end;
        for(int i = 0 ; i < itemsCurrUsed; i++){
            itemArray[i] = idxItem.value;
            idxItem = idxItem.next;
        }
        return itemArray;
    }
    // Returns array of String only of priority items in FIFO.

    public String[] toArrayOnlyPriority() {
        String[] itemArray = new String[itemsCurrUsed];
        Item idxItem = end;
        int count = 0;
        for(int i = 0 ; i < itemsCurrUsed; i++){
            if(idxItem.priority){
                itemArray[count] = idxItem.value;
                count++;
            }
            idxItem = idxItem.next;
        }
        
        String[] itemNewArray = new String[count];
        
        for(int i = 0 ; i < count ; i++){
            itemNewArray[i] = itemArray[i];
        }
        return itemNewArray;
    }

    // Items with priority=true have "*" appended, e.g. "[Jerry*,Maria,Tom*];
    // The item that get() will retrieve next is at the end.
    @Override
    public String toString() {
        String str = "[";
        Item idxItem = end;
        for(int i=0;i<itemsCurrUsed;i++){
            str += (i == 0)? "":", ";
            str += idxItem.value;
            str += idxItem.priority? "*":"";
            idxItem = idxItem.next;
        }
        str += "]";
        return str;
    }
}
