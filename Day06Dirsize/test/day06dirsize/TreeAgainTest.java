/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day06dirsize;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1898918
 */
public class TreeAgainTest {

    TreeAgain tree;

    @Before
    public void setUp() {
        tree = new TreeAgain();
    }

    @After
    public void tearDown() {
        tree = null;
    }

    @Test
    public void putNewNodeIntoTree() {
        try {
            int[] intArray = new int[]{5, 2, 7, 5, 1};
            for (int i = 0; i < 5; i++) {
                tree.put(intArray[i]);
                assertEquals((i + 1),tree.getNodesCount());
            }
            
            assertTrue("This should pass",true);
        } catch (DuplicateValueException ex) {
            assertFalse("this shouldn't happen",true);
        }
    }

}
