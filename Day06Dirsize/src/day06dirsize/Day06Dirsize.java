
package day06dirsize;

import java.io.File;

public class Day06Dirsize {
    
    static long getTotalDirSizeInBytes(File directory){
        long fileSize = 0;
        if(directory.listFiles() == null){
            return 0;
        }
        
        for(File f : directory.listFiles()){
            if(f.isDirectory()){
                fileSize += getTotalDirSizeInBytes(f);
            } else if(f.isFile()){
                fileSize += f.length();
            } else {
                System.out.println("Hello!");
            }
        }
        return fileSize;
    }
    static long getDirSizeInBytes(File directory){
        long fileSize = 0;
        for(File f : directory.listFiles()){
            if(f.isFile()){
                fileSize += f.length();
            }
        }
        return fileSize;
    }
    
    static final String FILE_NAME = "C:\\Windows";
    public static void main(String[] args) {
        System.out.printf("%s- file size: %,d Bytes\n",FILE_NAME ,getDirSizeInBytes(new File(FILE_NAME)));
        System.out.printf("%s- file size: %,d Bytes\n",FILE_NAME ,getTotalDirSizeInBytes(new File(FILE_NAME)));
    }
    
}
