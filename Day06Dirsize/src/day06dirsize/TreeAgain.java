
package day06dirsize;

class DuplicateValueException extends Exception {
    public DuplicateValueException(String msg){
        super(msg);
    }
}

public class TreeAgain {
    private class NodeOfInt {
        NodeOfInt left,right,parent;
        int value;
    }
    
    NodeOfInt root;
    int nodesCount;
    
    void put(int value) throws DuplicateValueException {
        if(root == null){
            NodeOfInt newNode = new NodeOfInt();
            newNode.value = value;
            nodesCount++;
            return;
        }
        
        NodeOfInt newNode = new NodeOfInt();
        
        NodeOfInt idxNode = root;
        boolean left = true;
        while(true){
            
            if(idxNode.value == value){ 
                throw new DuplicateValueException("Value is already in the tree"); 
            }else if( value < idxNode.value){
                left = true;
                if(idxNode.left == null){ 
                    idxNode.left = newNode;
                    break;
                }
            } else{
                left = false;
                if(idxNode.right == null) {
                    idxNode.right = newNode;
                    break;
                }
            }
            
        }
        nodesCount++;
    }
    
    int getNodesCount() { return nodesCount; }
    
    int getSumOfAllValues() {
        int sum = 0;
        if(root == null){return 0;}
        
        NodeOfInt idxNode = root;
        
        do{
            sum += idxNode.value;
            idxNode = idxNode.left;
        }while(idxNode.left != null);
        
        return sum;
    }
}
