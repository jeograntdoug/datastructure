/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day01arraylistownimpl;

import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author 1898918
 */
public class CustomArrayOfIntsTest {
    
    public CustomArrayOfIntsTest() {
    }
    
  
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of size method, of class CustomArrayOfInts.
     */
    @Test
    public void testSize() {
        System.out.println("size");
        CustomArrayOfInts instance = new CustomArrayOfInts();
        int expResult = 0;
        int result = instance.size();
        assertEquals(expResult, result);
   }

    @Test
    public void testAddRemoveContent() {
        CustomArrayOfInts instance = new CustomArrayOfInts();
        instance.add(2);
        instance.add(5);// delete
        instance.add(4);
        instance.add(7);// delete
        instance.add(1);
        instance.deleteByIndex(1);
        instance.deleteByIndex(2);
        assertEquals(3, instance.size());
        assertArrayEquals(new int[]{2,4,1}, instance.getSlice(0, instance.size()));
    }
    
}
