
package day01arraylistownimpl;

import org.junit.*;
import org.junit.Assert.*;


public class CustomArrayOfStringsTest {
    @Test
    public void testDeleteByValue(){
        System.out.println("Test: deleteByValue Method");
        CustomArrayOfStrings instance = new CustomArrayOfStrings();
        instance.add("A");
        instance.add("B");
        instance.add("C");
        instance.add("D");
        instance.add("E");
        Assert.assertArrayEquals(new String[]{"A","B","C","D","E"},instance.getSlice(0,instance.size()));
    }
}
