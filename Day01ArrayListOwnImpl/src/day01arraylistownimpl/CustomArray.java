
package day01arraylistownimpl;


public class CustomArray<T> {
    
	private T[] data = (T[]) new Object[1]; // only grows by doubling size, never shrinks
	private int size = 0;

	public int size() { return this.size; }
                
	public void add(T value) {
            if(size == data.length){
                expendDataArray();
            }
            data[size] = value;
            size++;
        }
        
        private void expendDataArray(){
            T[] newData = (T[])new Object[data.length*2];
            
            for(int index = 0; index < data.length ; index++){
                newData[index] = data[index];
            }
            data = newData;
        }
        
	public void deleteByIndex(int index) {
            if(index >= size  || index < 0){
                throw new ArrayIndexOutOfBoundsException("Array Index out of boundary");
            }
            
            for(int i = index + 1 ; i < size ; i++){
                data[i - 1] = data[i];
            }
            size--;
        }
        
	public boolean deleteByValue(T value) {
            for(int index = 0; index < size; index++){
                if(data[index].equals(value)){ 
                    deleteByIndex(index);
                    return true; 
                }
            }
            
            return false;
        } // delete first value matching
	
        public void insertValueAtIndex(T value, int index) {
            if(index > size || index < 0){
                throw new ArrayIndexOutOfBoundsException("Array Index out of boundary");
            }
            
            if(size == data.length){
                expendDataArray();
            }
            
            for(int i = size ; i > index; i--){
                data[i] = data[i-1];
            }
            
            data[index] = value;
            size++;
        }
	
        public void clear() {
            size = 0;
        }
        
	public T get(int index) {
            if(index >= size || index < 0){
                throw new ArrayIndexOutOfBoundsException("Array Index out of boundary");
            }
            
            return data[index];
        }
        
	public T[] getSlice(int startIdx, int length) {
            if(startIdx >= size || startIdx < 0){
                throw new ArrayIndexOutOfBoundsException("Array Index out of boundary");
            }
            if(length < 0 || startIdx + length > size){
                throw new ArrayIndexOutOfBoundsException("Array Index out of boundary");
            }
            
            T[] slice = (T[])new Object[length];
            int sliceIdx = 0;
            
            for(int index = startIdx; index < startIdx + length;index++ ){
                slice[sliceIdx++] = data[index];
            }
            
            return slice;
        }
	
	@Override
	public String toString() {
            String elementsStr = "[";
            for(int i = 0; i < size; i++){
                elementsStr += (i == 0) ? "" : ", ";
                elementsStr += data[i];
            }
            elementsStr += "]";
            return elementsStr;
        }    
}
