
package day01arraylistownimpl;


public class CustomArrayOfStrings {
    
	private String [] data = new String[1]; // only grows by doubling size, never shrinks
	private int size = 0;

	public int size() { return this.size; }
                
	public void add(String value) {
            if(size == data.length){
                expendDataArray();
            }
            data[size] = value;
            size++;
        }
        
        private void expendDataArray(){
            String[] newData = new String[data.length*2];
            
            for(int index = 0; index < data.length ; index++){
                newData[index] = data[index];
            }
            data = newData;
        }
        
	public void deleteByIndex(int index) {
            if(index >= size  || index < 0){
                throw new ArrayIndexOutOfBoundsException("Array Index out of boundary");
            }
            
            for(int i = index + 1 ; i < size ; i++){
                data[i - 1] = data[i];
            }
            size--;
        }
        
	public boolean deleteByValue(String value) {
            for(int index = 0; index < size; index++){
                if(data[index].equals(value)){ 
                    deleteByIndex(index);
                    return true; 
                }
            }
            
            return false;
        } // delete first value matching
	
        public void insertValueAtIndex(String value, int index) {
            if(index > size || index < 0){
                throw new ArrayIndexOutOfBoundsException("Array Index out of boundary");
            }
            
            if(size == data.length){
                expendDataArray();
            }
            
            for(int i = size ; i > index; i--){
                data[i] = data[i-1];
            }
            
            data[index] = value;
            size++;
        }
	
        public void clear() {
            size = 0;
        }
        
	public String get(int index) {
            if(index >= size || index < 0){
                throw new ArrayIndexOutOfBoundsException("Array Index out of boundary");
            }
            
            return data[index];
        }
        
	public String[] getSlice(int startIdx, int length) {
            if(startIdx >= size || startIdx < 0){
                throw new ArrayIndexOutOfBoundsException("Array Index out of boundary");
            }
            if(length < 0 || startIdx + length > size){
                throw new ArrayIndexOutOfBoundsException("Array Index out of boundary");
            }
            
            String[] slice = new String[length];
            int sliceIdx = 0;
            
            for(int index = startIdx; index < startIdx + length;index++ ){
                slice[sliceIdx++] = data[index];
            }
            
            return slice;
        }
	
	@Override
	public String toString() {
            String elementsStr = "[";
            for(int i = 0; i < size; i++){
                elementsStr += (i == 0) ? "" : ", ";
                elementsStr += data[i];
            }
            elementsStr += "]";
            return elementsStr;
        }
}
