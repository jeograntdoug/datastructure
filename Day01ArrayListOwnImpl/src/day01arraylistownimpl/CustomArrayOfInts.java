
package day01arraylistownimpl;


public class CustomArrayOfInts {
    
	private int [] data = new int[1]; // only grows by doubling size, never shrinks
	private int size = 0;

	public int size() { return this.size; }
                
	public void add(int value) {
            if(size == data.length){
                expendDataArray();
            }
            data[size] = value;
            size++;
        }
        private void expendDataArray(){
            int[] newData = new int[data.length*2];
            
            for(int index = 0; index < data.length ; index++){
                newData[index] = data[index];
            }
            data = newData;
        }
        
	public void deleteByIndex(int index) {
            if(index >= size  || index < 0){
                throw new ArrayIndexOutOfBoundsException("Array Index out of boundary");
            }
            
            for(int i = index + 1 ; i < size ; i++){
                data[i - 1] = data[i];
            }
            size--;
        }
        
	public boolean deleteByValue(int value) {
            for(int index = 0; index < size; index++){
                if(data[index] == value){ 
                    deleteByIndex(index);
                    return true; 
                }
            }
            return false;
        } // delete first value matching
	
        public void insertValueAtIndex(int value, int index) {
            if(index > size || index < 0){
                throw new ArrayIndexOutOfBoundsException("Array Index out of boundary");
            }
            
            if(size == data.length){
                expendDataArray();
            }
            
            for(int i = size ; i > index; i--){
                data[i] = data[i-1];
            }
            
            data[index] = value;
            size++;
        }
	
        public void clear() {
            size = 0;
        }
        
	public int get(int index) {
            if(index >= size || index < 0){
                throw new ArrayIndexOutOfBoundsException("Array Index out of boundary");
            }
            
            return data[index];
        }
        
	public int[] getSlice(int startIdx, int length) {
            if(startIdx >= size || startIdx < 0){
                throw new ArrayIndexOutOfBoundsException("Array Index out of boundary");
            }
            if(length < 0 || startIdx + length > size){
                throw new ArrayIndexOutOfBoundsException("Array Index out of boundary");
            }
            
            int[] slice = new int[length];
            int sliceIdx = 0;
            
            for(int index = startIdx; index < startIdx + length;index++ ){
                slice[sliceIdx++] = data[index];
            }
            
            return slice;
        }
	
	@Override
	public String toString() {
            String[] dataStrArray = new String[size];
            
            for(int i = 0; i < size; i++){
                dataStrArray[i] = data[i] + "";
            }
            return String.join(",",dataStrArray);
        } // returns String similar to: [3, 5, 6, -23]
}
