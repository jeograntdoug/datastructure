/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day01arraylistownimpl;

import java.util.Scanner;

/**
 *
 * @author heokc
 */
public class Day01ArrayListOwnImpl {
    public static Scanner input = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Test(1:intArray,2:stringArray,3:genetic):");
        int choice = input.nextInt();
        input.nextLine();
       
        switch(choice){
            case 1:
                testInteger();
                break;
            case 2:
                testString();
                break;
            case 3:
                testGenetic();
                break;
            default :
                System.out.println("No valid menu. ByeBye~");
        }
    }
    
    public static void testString(){
        CustomArrayOfStrings arrayStrings = new CustomArrayOfStrings();

        String[] testArray = {"a","b","c","d"};
        for(int i=0;i < testArray.length; i++){
            arrayStrings.add(testArray[i]);
        }

        System.out.println("valueArray:"+ String.join(",",testArray));
        System.out.println("arrayInt:" + arrayStrings.toString());

        System.out.print("Enter delete val: ");
        String value = input.nextLine();
        if(!arrayStrings.deleteByValue(value)){
            System.out.println("no value in array");
        } else{
            System.out.println("arrayInt:" + arrayStrings.toString());
        }

        System.out.print("Enter add val: ");
        value = input.nextLine();
        System.out.print("Enter index: ");
        int index = input.nextInt();
        input.nextLine();
        arrayStrings.insertValueAtIndex(value,index);
        System.out.println("arrayInt:" + arrayStrings.toString());
    }
    
    public static void testInteger(){
        int ranSizeOfArray = (int)(Math.random()*10) + 1;
        CustomArrayOfInts arrayInt = new CustomArrayOfInts();

        String[] valueArray = new String[ranSizeOfArray];
        for(int i=0;i < ranSizeOfArray; i++){

            int ranVal = (int)(Math.random() * 20);
            arrayInt.add(ranVal);
            valueArray[i] = ranVal + "";
        }

        System.out.println("valueArray:"+ String.join(",",valueArray));
        System.out.println("arrayInt:" + arrayInt.toString());

        System.out.print("Enter index to get: ");
        int index = input.nextInt();
        input.nextLine();
        System.out.println("value: " + arrayInt.get(index));

        System.out.print("Enter index to remove: ");
        index = input.nextInt();
        input.nextLine();
        arrayInt.deleteByIndex(index);
        System.out.printf("new Array: [%s] size: %d",arrayInt.toString(),arrayInt.size());

    }
    
    public static void testGenetic(){
            CustomArray<String> arrayStrings = new CustomArray();

            String[] testArray = {"a","b","c","d"};
            for(int i=0;i < testArray.length; i++){
                arrayStrings.add(testArray[i]);
            }

            System.out.println("valueArray:"+ String.join(",",testArray));
            System.out.println("arrayInt:" + arrayStrings.toString());

            System.out.print("Enter delete val: ");
            String value = input.nextLine();
            if(!arrayStrings.deleteByValue(value)){
                System.out.println("no value in array");
            } else{
                System.out.println("arrayInt:" + arrayStrings.toString());
            }
            
            System.out.print("Enter add val: ");
            value = input.nextLine();
            System.out.print("Enter index: ");
            int index = input.nextInt();
            input.nextLine();
            arrayStrings.insertValueAtIndex(value,index);
            System.out.println("arrayInt:" + arrayStrings.toString());
    }
    
}
