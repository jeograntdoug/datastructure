
import day02cachingfibonacci.FibCached;
import static org.junit.Assert.*;
import org.junit.Test;



public class FibonacciTest {
    @Test
    public void fibCachedHasInitialValuesAndCompCount(){
        FibCached fib = new FibCached();
        // Initial value: index[0] - 0, index[1] - 1
        assertEquals(0,fib.getNthFib(0));
        assertEquals(fib.getCountOfFibsComputed(),2);
        assertEquals(1,fib.getNthFib(1));
        assertEquals(fib.getCountOfFibsComputed(),2);
    }
    
    @Test
    public void fibonacciThrowExceptionByInvalidIndex(){
        FibCached fib = new FibCached();
        //Invalid index -1
        try{
            long val = fib.getNthFib(-1);
            assertTrue("This message must not be shown",false);
        } catch(IndexOutOfBoundsException ex){
            assertTrue("IndexOutOfBoundsException works",true);
        }
    }
    
    @Test
    public void compCountIncreasesWhenItCalculatesNewNthIndex(){
        FibCached fib = new FibCached();
        // Check default computedCount = 2 / index 0,1
        int oldCompCount = fib.getCountOfFibsComputed();
        fib.getNthFib(0);
        fib.getNthFib(1);
        assertEquals(oldCompCount, fib.getCountOfFibsComputed());
        
        fib.getNthFib(10);
        int expectedCount = 10 - 1 + oldCompCount;
        assertEquals(expectedCount,fib.getCountOfFibsComputed());
    }
    
    @Test
    public void compCountMustNotIncreaseWhenProgramAccessExistingValue(){
        FibCached fib = new FibCached();
        fib.getNthFib(20);
        int compCount = fib.getCountOfFibsComputed();
        for(int i = 0; i <= 20 ; i++){
            fib.getNthFib(i);
        }
        assertEquals(compCount,fib.getCountOfFibsComputed());
    }
    
    
}
