
package day02cachingfibonacci;

import java.util.ArrayList;

public class FibCached {
    public FibCached() {
            fibsCached.add(0L); // #0
            fibsCached.add(1L); // #1
    }

    private ArrayList<Long> fibsCached = new ArrayList<>();
    private int fibsCompCount = 2;
    // in a correct caching implementation fibsCompCount will end up the same as fibsCached.size();

    public long getNthFib(int n) {
        if(n < 0){
            throw new IndexOutOfBoundsException();
        }
        if( n >= fibsCached.size()){
            //return computeNthFib(n);
            return computeNthFibRecursive(n);
        }
        return fibsCached.get(n);
    }

    // You can find implementation online, recursive or non-recursive.
    // For 100% solution you should use values in fibsCached as a starting point
    // instead of always starting from the first two values of 0, 1.
    private long computeNthFib(int n) {
        for(int i = fibsCached.size(); i <= n ; i++){
            long fib = fibsCached.get(i-2) + fibsCached.get(i-1);
            fibsCached.add(fib);
            fibsCompCount++;
        }
        return fibsCached.get(n);
    }
    
    
    private long computeNthFibRecursive(int n){
        if(n < fibsCached.size()){ return fibsCached.get(n); }
        long sum = computeNthFibRecursive(n-2) + computeNthFibRecursive(n-1);
        fibsCached.add(sum);
        fibsCompCount++;
        return sum;
    }
    // You are allowed to add another private method for fibonacci generation
    // if you want to use recursive approach. I recommend non-recursive though.

    // How many fibonacci numbers has your code computed as opposed to returned cached?
    // Use this in your testing to make sure your caching actually works properly.
    public int getCountOfFibsComputed() {
        return fibsCompCount;
    }

    @Override
    public String toString() { // returns all cached Fib values, comma-space-separated
        return fibsCached.toString();
    }

}
