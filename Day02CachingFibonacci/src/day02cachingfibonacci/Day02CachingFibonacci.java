package day02cachingfibonacci;

public class Day02CachingFibonacci {

    public static void main(String[] args) {
        FibCached fibonacci = new FibCached();

        for (int i = 0; i < 20; i++) {
            System.out.printf("Fibonacci[%d]:%d / computing count:%d/ %s \n", i, fibonacci.getNthFib(i), fibonacci.getCountOfFibsComputed(), fibonacci);
        }
        for (int i = 0; i < 20; i++) {
            System.out.printf("Fibonacci[%d]:%d / computing count:%d/ %s \n", i, fibonacci.getNthFib(i), fibonacci.getCountOfFibsComputed(), fibonacci);
        }

    }

}
