/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day02wordsfreq;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 *
 * @author 1898918
 */
public class Day02WordsFreq {

    public static void main(String[] args) {
        System.out.println("Read bible with HashMap");
        try ( Scanner fileInput = new Scanner(new File("pg10.txt"))) {
            HashMap<String, Integer> map = new HashMap<>();
            fileInput.useDelimiter("[ \t\n\r\\.();:,?-]+");
            
            
            
            while (fileInput.hasNext()) {
                String word = fileInput.next();
                map.putIfAbsent(word, 0);
                map.put(word, map.get(word) + 1 );
            }
            
            long startTime = System.nanoTime();
            //TODO: test if key words are valid
            Set<Entry<String,Integer>> entries = map.entrySet();
            
            ArrayList<Entry<String,Integer>> wordEntityList = new ArrayList<>(entries);
            
            Collections.sort(wordEntityList,compareByValOfSet);
            
            LinkedHashMap<String,Integer> sortedSet = new LinkedHashMap<>();
            for(Entry<String,Integer> entry: wordEntityList){
                sortedSet.put(entry.getKey(), entry.getValue());
            }
            
            
            long endTime = System.nanoTime();
            
            int count = 1;
            for(Entry<String,Integer> entry: sortedSet.entrySet()){
                if(count > 3){
                    break;
                }
                System.out.printf("%d: \"%s\" occured %d times\n",count,entry.getKey(),entry.getValue());
                count++;
            }
            double sortingTime = ( endTime - startTime ) / 1000.0;
            System.out.printf("the amount of time spent sorting : %.3fmicro s\n",sortingTime);

            
        } catch (IOException ex) {
            System.out.println("Fail to open file");
        }
        
        System.out.println("");
        
        
        // Tree Map
        // Tree Map sorting
        
        System.out.println("Read Bible with Tree Map");
        try ( Scanner fileInput = new Scanner(new File("pg10.txt"))) {
            HashMap<String, Integer> map = new HashMap<>();
            fileInput.useDelimiter("[ \t\n\r\\.();:,?-]+");
            
            
            while (fileInput.hasNext()) {
                String word = fileInput.next();
                map.putIfAbsent(word, 0);
                map.put(word, map.get(word) + 1 );
            }
           
            //TODO: test if key words are valid
            long startTime = System.nanoTime();
            
            TreeMap<Integer,String> tMap = new TreeMap<>(Collections.reverseOrder());
            
            for(Entry<String,Integer> e : map.entrySet()){
                tMap.put(e.getValue(), e.getKey());
            }
            
            long endTime = System.nanoTime();
            
            int count = 1;
            for(Entry<Integer,String> entry: tMap.entrySet()){
                if(count > 3){
                    break;
                }
                System.out.printf("%d: \"%s\" occured %d times\n",count,entry.getValue(),entry.getKey());
                count++;
            }
            double sortingTime = ( endTime - startTime )/1000.0;
            System.out.printf("the amount of time spent sorting : %.3fmicro s\n",sortingTime);

            
        } catch (IOException ex) {
            System.out.println("Fail to open file");
        }
        
        System.out.println("");
        
        
        // Tree Set
        // Tree Set sorting
        //https://stackoverflow.com/questions/2864840/treemap-sort-by-value
        
        System.out.println("Read Bible with Tree Set");
        try ( Scanner fileInput = new Scanner(new File("pg10.txt"))) {
            HashMap<String, Integer> map = new HashMap<>();
            fileInput.useDelimiter("[ \t\n\r\\.();:,?-]+");
            
            while (fileInput.hasNext()) {
                String word = fileInput.next();
                map.putIfAbsent(word, 0);
                map.put(word, map.get(word) + 1 );
            }
           
            //TODO: test if key words are valid
            long startTime = System.nanoTime();
            
            
            SortedSet<Entry<String,Integer>> sortedMap = new TreeSet<Entry<String,Integer>>(compareByValOfSet);
            //sortedMap.putAll(map);
            sortedMap.addAll(map.entrySet());
            long endTime = System.nanoTime();
            
            int count = 1;
            for(Entry<String,Integer> entry: sortedMap){
                if(count > 3){
                    break;
                }
                System.out.printf("%d: \"%s\" occured %d times\n",count,entry.getKey(),entry.getValue());
                count++;
            }
            double sortingTime = ( endTime - startTime ) / 1000.0;
            System.out.printf("the amount of time spent sorting : %.3fmicro s\n",sortingTime);

            
        } catch (IOException ex) {
            System.out.println("Fail to open file");
        }
    }
    
    public static Comparator<Entry<String,Integer>> compareByValOfSet = new Comparator<Entry<String,Integer>>(){
        @Override
        public int compare(Entry<String,Integer> entry1, Entry<String,Integer> entry2){
            return entry2.getValue() - entry1.getValue();
        }
    };
    
  
   
}
