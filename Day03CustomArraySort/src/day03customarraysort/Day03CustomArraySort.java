
package day03customarraysort;

public class Day03CustomArraySort {
    public static void main(String[] args) {
        
        //int [] data = {3,7,11,5,3,8,20,17,5,7};
        
        int[] data;
        // array size = 10
        data = makeRandomArray(10);
        arraySort(data);
        
        // array size = 100
        data = makeRandomArray(100);
        arraySort(data);
        
        // array size = 1000
        data = makeRandomArray(1000);
        arraySort(data);
        
        // array size = 10000
        data = makeRandomArray(10000);
        arraySort(data);
    }
    static int[] makeRandomArray(int size){
        int[] data = new int[size];
        
        for(int i = 0 ; i < data.length; i++){
            data[i] = (int)(Math.random()*1000);
        }
        return data;
    }
    
    static void arraySort(int[] data){
        
        boolean isSwitch = true;
        int count = 0;
        while(isSwitch){
            
            isSwitch = false;
            
            for(int i = 1 ; i < data.length ; i++){
                count++;
                if(data[i] < data[i-1]){
                    int temp = data[i];
                    data[i] = data[i-1];
                    data[i-1] = temp;
                    isSwitch = true;
                }
            }
        }
        
        System.out.printf(
                "%5d array search count:%,15d\n",
                data.length,
                count
        );
    }
    
}
