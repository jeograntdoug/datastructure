package day01arraysearch;

import java.util.ArrayList;
import java.util.Scanner;

public class Day01ArraySearch {

    static Scanner input = new Scanner(System.in);

    static int sumOfCross(int[][] data, int row, int col) {
        // return sum of the element at row/col
        // plus (if they exist) element above, below, to the left and right of it
//        if(row == 0){}// only down
//        if(row == data.length){}//only up
//        if(col == 0){}// only right
//        if(col == data.length){}// only left

        int sum = getIfExistGentle(data, row, col)
                + getIfExistGentle(data, row - 1, col)
                + getIfExistGentle(data, row + 1, col)
                + getIfExistGentle(data, row, col - 1)
                + getIfExistGentle(data, row, col + 1);
        return sum;
    }

    static int getIfExistGentle(int[][] data, int row, int col) {
        if (row >= data.length || row < 0) {
            return 0;
        }
        if (col >= data[row].length || col < 0) {
            return 0;
        }
        return data[row][col];
    }

    static int[][] duplicateArray2D(int[][] orig2D) {
        int[][] dup2D = new int[orig2D.length][];
        for (int row = 0; row < orig2D.length; row++) {
            dup2D[row] = new int[orig2D[row].length];
        }
        return dup2D;
    }

    static void printArray2D(int[][] data2D) {
        int maxChars = 1;
        for (int row = 0; row < data2D.length; row++) {
            for (int col = 0; col < data2D[row].length; col++) {
                String valStr = data2D[row][col] + "";
                if (maxChars < valStr.length()) {
                    maxChars = valStr.length();
                }
            }
        }

        for (int row = 0; row < data2D.length; row++) {
            System.out.print("[");
            for (int col = 0; col < data2D[row].length; col++) {
                System.out.printf("%s%" + maxChars + "d", (col == 0) ? "" : ",", data2D[row][col]);
            }
            System.out.println("]");
        }
    }

    public static void main(String[] args) {
        int[][] data2D = {
            {1, 3, 6, 8, 10},
            {7, 1, 2},
            {8, 3, 2, 1, 10, 20},
            {1, 7, 1, 9},};

        // Using sumOfCross() method write a search that will find
        // which element at row/col has the smallest sum of itself
        // and elements surrounding it.
        int minSum = sumOfCross(data2D, 0, 0);
        int rowMin = 0;
        int colMin = 0;

        for (int row = 0; row < data2D.length; row++) {
            for (int col = 0; col < data2D[row].length; col++) {
                if (getIfExistGentle(data2D, row, col) == 0) {
                    continue;
                }

                int sum = sumOfCross(data2D, row, col);
                if (sum < minSum) {
                    minSum = sum;
                    rowMin = row;
                    colMin = col;
                }
            }
        }

        System.out.printf("Minimun sum of cross value is %d, row/col is %d/%d\n",
                minSum, rowMin, colMin);
        System.out.println("");

        //////////////////////
        int[][] newArray = new int[data2D.length][];

        for (int row = 0; row < data2D.length; row++) {
            newArray[row] = new int[data2D[row].length];
            for (int col = 0; col < data2D[row].length; col++) {
                newArray[row][col] = sumOfCross(data2D, row, col);
            }
        }
        printArray2D(newArray);

        System.out.println("");

        /////////////////////////////
        System.out.println("Original Array");
        printArray2D(data2D);

        // Part3:
        System.out.print("Enter sum to find in array:");
        int value = input.nextInt();
        input.nextLine();
//        for (int row = 0; row < data2D.length; row++) {
//            for (int col = 0; col < data2D[row].length; col++) {
//                
//                for (int nextRow = row; nextRow < data2D.length; nextRow++) {
//                    for (int nextCol = 0; nextCol < data2D[nextRow].length; nextCol++) {
//                        if(row == nextRow && col == nextCol){ continue; }
//                        if(row == nextRow && col > nextCol){ continue; }
//                        
//                        int sum = data2D[row][col] + data2D[nextRow][nextCol];
//                        
//                        if ( sum == value ) {
//                            System.out.printf(
//                                "[%d,%d]:%d + [%d,%d]:%d = %d\n",
//                                row, col, data2D[row][col],
//                                nextRow, nextCol, data2D[nextRow][nextCol],
//                                sum
//                            );
//                        }
//                    }
//                }
//
//            }
//        }
        
////    Complexcity = O(N^2)
        System.out.println("==================================");
        System.out.println("Count Operation");
        countOperation(4,5,value);
        countOperation(20,10,value);
        countOperation(50,40,value);
        countOperation(50,400,value);
        countOperation(500,400,value);
        
        
    }
    static void countOperation(int r,int c,int value){
       
        long count=0;
        int[][] data2D = new int[r][c];

        
        for (int row = 0; row < data2D.length; row++) {
            for (int col = 0; col < data2D[row].length; col++) {
                
                for (int nextRow = row; nextRow < data2D.length; nextRow++) {
                    for (int nextCol = 0; nextCol < data2D[nextRow].length; nextCol++) {
                        if(row == nextRow && col == nextCol){ continue; }
                        if(row == nextRow && col > nextCol){ continue; }
                        
                        int sum = data2D[row][col] + data2D[nextRow][nextCol];
                        count++;
                        if ( sum == value ) {
//                            System.out.printf(
//                                "[%d,%d]:%d + [%d,%d]:%d = %d\n",
//                                row, col, data2D[row][col],
//                                nextRow, nextCol, data2D[nextRow][nextCol],
//                                sum
//                            );
                        }
                    }
                }

            }
        }
        System.out.printf("%4d x %4d array search count:%,15d\n",data2D.length,data2D[0].length,count);
    }
}
