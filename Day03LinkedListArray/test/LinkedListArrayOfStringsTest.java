
import day03linkedlistarray.LinkedListArray;
import day03linkedlistarray.LinkedListArrayOfStrings;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class LinkedListArrayOfStringsTest {
    LinkedListArray<String> linkedList;
    @Before
    public void setUp() {
        linkedList = new LinkedListArray<>();
        linkedList.add("First");
        linkedList.add("Second");
        linkedList.add("Third");
    }
    
    
    @Test
    public void linkedListCanAddElement(){
        
        assertEquals("First", linkedList.get(0));
        assertEquals("Second", linkedList.get(1));
        assertEquals("Third", linkedList.get(2));
        assertEquals(3,linkedList.getSize());
        
        linkedList.deleteByIndex(2);
        linkedList.deleteByIndex(1);
        linkedList.deleteByIndex(0);
        assertEquals(0, linkedList.getSize());
        
        try{
            linkedList.deleteByIndex(0);
            assertTrue("This message must not shown",false);
        } catch (IndexOutOfBoundsException ex){
            assertTrue("IndexOutOfBoundsException works",true);
        }
        
        linkedList.add("First");
        linkedList.add("Second");
        linkedList.add("Third");
        assertEquals("First", linkedList.get(0));
        assertEquals("Second", linkedList.get(1));
        assertEquals("Third", linkedList.get(2));
        assertEquals(3,linkedList.getSize());
    }
    
    @Test
    public void linkedListCanDeleteElementByIndex(){
        
        assertEquals(3, linkedList.getSize());
        
        linkedList.deleteByIndex(2);
        linkedList.deleteByIndex(1);
        linkedList.deleteByIndex(0);
        assertEquals(0, linkedList.getSize());
        
        linkedList.add("Forth");
        assertEquals("Forth",linkedList.get(0));
        assertEquals(1,linkedList.getSize());
    }
    
    
    @Test
    public void linkedListCanDeleteElementByValue(){
        
        assertFalse(linkedList.deleteByValue("irst"));
        assertEquals(3,linkedList.getSize());
        
        assertTrue(linkedList.deleteByValue("Second"));
        assertEquals(2,linkedList.getSize());
    }
    
    @Test
    public void linkedListCanInsertElement(){
        LinkedListArrayOfStrings linkedList = new LinkedListArrayOfStrings();
        linkedList.add("Third");
        linkedList.insertValueAtIndex(0, "First");
        linkedList.insertValueAtIndex(1, "Second");
        linkedList.insertValueAtIndex(linkedList.getSize(), "Forth");
        
        assertEquals("First",linkedList.get(0));
        assertEquals("Second",linkedList.get(1));
        assertEquals("Third",linkedList.get(2));
        assertEquals("Forth",linkedList.get(3));
        assertEquals(4, linkedList.getSize());
    }
}