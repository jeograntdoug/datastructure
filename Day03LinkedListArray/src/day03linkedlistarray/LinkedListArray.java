
package day03linkedlistarray;

import java.util.Arrays;

public class LinkedListArray<T> {
    private class Container<T> {
        Container next;
        T value;
    }
    
    Container start, end;
    int size = 0;

    public void add(T value) {
        Container newCont = new Container();
        newCont.value = value;
        newCont.next = null;
        
        if(start == null){
            start = newCont;
            end = newCont;
            size++;
            return;
        }
        
        end.next = newCont;
        end = newCont;
        size++;
    }
    
    public T get(int index) {
        if(index < 0 || index >= size){
            throw new IndexOutOfBoundsException();
        }
        
        if(index == size - 1){
            return (T)end.value;
        }
        
        Container cont = start;
        for(int i = 0 ; i < index ; i++){
            cont = cont.next;
        }
        
        return (T)cont.value;
    }
    
    public void insertValueAtIndex(int index, T value) {
        if(index < 0 || index > size){
            throw new IndexOutOfBoundsException();
        }
        
        if(index == size){ 
            add(value);
            return;
        }
        
        Container newCont = new Container();
        newCont.value = value;
        newCont.next = null;
        
        Container idxMark = start;
  
        if(index == 0){
            start = newCont;
            newCont.next = idxMark;
            size++;
            return;
        }
        
        Container prevIdxMark = getContainer(index - 1);
        
        idxMark = prevIdxMark.next;
        prevIdxMark.next = newCont;
        newCont.next = idxMark;
        size++;
    }
    
    private Container getContainer(int index){
        Container conIdx = start;
        for(int i = 0; i < index; i++){
            conIdx = conIdx.next;
        }
        return conIdx;
    }
    
    public void deleteByIndex(int index) { 
        if(index < 0 || index >= size){
            throw new IndexOutOfBoundsException();
        }
        if(index == 0){
            start = start.next;
            size--;
            if(start == null){ end = null; }
            return;
        }
        
        Container prevIdxCont = getContainer(index - 1);
        
        if(prevIdxCont.next == end){
            end = prevIdxCont;
        }
        prevIdxCont.next = prevIdxCont.next.next;
        
        size--;
    }
    
    public boolean deleteByValue(T value) {// delete first value found
        if(size == 0){ return false; }
        
        if(start.value.equals(value)){
            start = start.next;
            size--;
            return true;
        }
        
        Container idxCont = start;
        
        for(int i=0; i < size - 1  ; i++){
            if(idxCont.next.value.equals(value)){
                if(idxCont.next == end){
                    end = idxCont;
                }
                idxCont.next = idxCont.next.next;
                size--;
                return true;
            }
            idxCont =  idxCont.next;
        }
        
        
        return false;
    } 
    
    public int getSize() {
        return size;
    }
    

    @Override
    public String toString() {// comma-separated values list
        return Arrays.toString(toArray());
    } 

    public T[] toArray() { // could be used for Unit testing
        T[] dataArray = (T[])new Object[size];
        Container idxCont = start;
        for(int i=0; i<size ; i++){
            dataArray[i] = (T)idxCont.value;
            idxCont = idxCont.next;
        }
        return dataArray;
    } 
}
