
package day08patternobserver;

public class Day08PatternObserver {

    public static void main(String[] args) {
        
        
        SimpleTreeOfUniqueStrings tree = new SimpleTreeOfUniqueStrings();
        
//        tree.setObserver(new SimpleTreeOfUniqueStrings.NodeModiricationObserver() {
//            @Override
//            public void nodeModified(SimpleTreeOfUniqueStrings.Node node, SimpleTreeOfUniqueStrings.NodeModiricationObserver.Action action) {
//                System.out.printf("Node: %s is %s\n",node.toString(),action);
//            }
//        }); 
        
        tree.setObserver((node, action) -> {
            System.out.printf("Node: %s is %s\n",node.toString(),action);
        });
        
        for( int n : new int[]{10,7,12,15,8,5,3,11}){
           tree.add(n + "");
        }
        tree.printAll();
        
        
    }
    
}
